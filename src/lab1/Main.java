package lab1;

/*

Лаба 1.
Написать метод, который выводит true, если значение аргументов x и y, находится строго между 0 и 1, и false в остальных случаях.
Написать метод, который выводит двоичное представление положительного целого числа в переменную типа строки.
Написать статичный метод lg, который принимает аргумент типа int и возвращает наибольшее целое число не большем, чем двоичный логарифм N. Math использовать нельзя.

 */

public class Main {

    public static void main(String[] args) {
        System.out.println("0.45 and 0.99 is between 0 and 1: " + between(0.45f, 0.99f));
        System.out.println("1.45 and 0.99 is between 0 and 1: " + between(1.45f, 0.99f));
        System.out.println("5 to binary: " + toBinaryString(-5));
        System.out.println("0 to binary: " + toBinaryString(0));
        System.out.println("144 lg: " + lg(144));
    }

    public static Boolean between(float x, float y) {
        return (x < 1 && x > 0) && (y < 1 && y > 0);
    }

    public static String toBinaryString(int x) {
        // return Integer.toBinaryString(x);

        StringBuilder result = new StringBuilder();

        do {
            result.insert(0, x % 2);
            x /= 2;
        } while (x > 0);

        return result.toString();
    }

    public static int lg(int x){
        int y = 2;
        int count = 0;
        while (y < x) {
            y *= 2;
            count++;
        }

        return count;
    }
}
