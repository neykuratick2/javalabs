package lab2;

/*

В текстовом файле находится матрица произвольного размера. Нужно считать её (можно пользоваться In и StdIn),
А далее отсортировать её в порядке слева направо, а также по спирали.
То есть элементы увеличиваются по спирали, например:
1 2 3 4
10 11 5
9 8 7 6
Провести оценку сложности построенного алгоритма.

 */

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        // читаем цифры с файла и конвертируем их в массив из интов
        String content = Files.readString(Paths.get("src/lab2/input.txt")).replaceAll("[\\r\\n]+", " ");
        String[] numbersRaw = content.split(" ");
        ArrayList<Integer> numbers = new ArrayList<>(Arrays.stream(numbersRaw).map(Integer::valueOf).toList());

        execute(numbers, true);
    }

    public static void execute(ArrayList<Integer> numbers, boolean print) {
        // сортируем и вычисляем размер массива
        Collections.sort(numbers);
        double size = Math.sqrt(numbers.size());
        int matrixLength = (int) Math.ceil(size);
        int matrixHeight = (int) Math.floor(size);

        // конвертируем массив в спиральную матрицу
        int[][] spiralMatrix = toSpiral(numbers, matrixLength,  matrixHeight);

        // принтим полученную матрицу
        if (!print) return;
        for (int i = 0; i < matrixLength; i++) {
            for (int j = 0; j < matrixHeight; j++) {
                System.out.print(spiralMatrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    static int[][] toSpiral(ArrayList<Integer> sorted, int matrixLength, int matrixHeight) {
        int[][] emptyMatrix = new int[matrixHeight + 1][matrixLength + 1];
        int currentRow = 0, currentColumn = 0, index = 0;

        while (currentRow < matrixLength && currentColumn < matrixHeight) {

            // первая строчка
            for (int i = currentColumn; i < matrixHeight; ++i) {
                emptyMatrix[currentRow][i] = sorted.get(index);
                index++;
            }

            currentRow++;

            // последняя колонка
            for (int i = currentRow; i < matrixLength; ++i) {
                emptyMatrix[i][matrixHeight - 1] = sorted.get(index);
                index++;
            }
            matrixHeight--;

            // последняя строчка
            if (currentRow < matrixLength) {
                for (int i = matrixHeight - 1; i >= currentColumn; --i) {
                    emptyMatrix[matrixLength - 1][i] = sorted.get(index);
                    index++;
                }
                matrixLength--;
            }

            // первая колонка
            if (currentColumn < matrixHeight) {
                for (int i = matrixLength - 1; i >= currentRow; --i) {
                    emptyMatrix[i][currentColumn] = sorted.get(index);
                    index++;
                }
                currentColumn++;
            }
        }

        return emptyMatrix;
    }
}





