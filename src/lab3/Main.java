package lab3;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Random;

/*

1. Произвести оценку временных затрат на выполнение модулей вашего алгоритма из 2 лабы.
2. Как сильно увеличатся затраты на выполнение алгоритма если размер матрицы будет 10, 50, 150?
Сделать тесты с соответствующими выводами, можно через коэффициенты.
Материал по 3 лекции. (использовалось на основе 2 лекции)

1 {
    main() - o(n) (наверное)
    execute() - o(n2)
    toSpiral() - o(n2)
}

2 {
    1_000, 2_000 (увеличение в 2 раза) - исполняется в 4 раза дольше - o(n2)
    200, 2_000 (увеличение в 10 раз) - исполняется в 108 раз дольше - o(n2)
    30, 3000 (увеличение в 100 раз) - исполняется в 5_325 раз дольше - o(2n)
    4, 4000 (увеличение в 1000 раз) - исполняется в ~105_000 раз дольше

    Следовательно, работает в o(n2)
}

 */


public class Main {
    public static void main(String[] args) {
        run(1_000, 2_000);
        System.out.println();
        run(200, 2_000);
        System.out.println();
        run(30, 3_000);
        System.out.println();
        run(4, 4_000);
    }

    public static void run(int matrixSize1, int matrixSize2) {
        long start = System.nanoTime();
        test(matrixSize1, matrixSize1);
        long stop = System.nanoTime();
        BigDecimal result_1000 = BigDecimal.valueOf((stop-start)/1000000.0);
        System.out.println(matrixSize1 + ": Time: " + result_1000 + " msec");


        start = System.nanoTime();
        test(matrixSize2, matrixSize2);
        stop = System.nanoTime();
        BigDecimal result_2000 = BigDecimal.valueOf((stop-start)/1000000.0);
        System.out.println(matrixSize2 + ": Time: " + result_2000 + " msec");

        double matrixDelta = (double) Math.max(matrixSize1, matrixSize2) / Math.min(matrixSize1, matrixSize2);
        BigDecimal resultDelta = result_2000.divide(result_1000, 2, RoundingMode.HALF_EVEN);

        System.out.printf(
                "При увеличении матрицы в %s раза время исполнения увеличивается в %s раз%n",
                matrixDelta,
                resultDelta
        );
    }

    public static void test(int matrixHeight, int matrixLength) {
        int minValue = 0;
        int maxValue =  9;
        Random rn = new Random();

        ArrayList<Integer> matrix = new ArrayList<>();
        for (int i = 0; i < matrixHeight * matrixLength; i++) {
            matrix.add(minValue + rn.nextInt(maxValue - minValue + 1));
        }

        lab2.Main.execute(matrix, false);
    }
}
